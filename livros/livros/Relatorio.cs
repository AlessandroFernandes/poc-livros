﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace livros
{
    public class Relatorio : IRelatorio
    {
        private readonly ICatalogo Catalogo;

        public Relatorio(ICatalogo catalogo)
        {
            Catalogo = catalogo;
        }
        public async Task RelatorioLivros(HttpContext context)
        {
            foreach (var livro in Catalogo.GetLivros())
            {
                await context.Response.WriteAsync($"{livro.Codigo,-5}{livro.Nome,-40}{livro.Preco.ToString("c"),-10}\r\n");
            }
        }
    }
}
