﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace livros
{
    public interface IRelatorio
    {
        Task RelatorioLivros(HttpContext context);
    }
}