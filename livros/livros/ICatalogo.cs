﻿using System.Collections.Generic;

namespace livros
{
    public interface ICatalogo
    {
        List<Livro> GetLivros();
    }
}