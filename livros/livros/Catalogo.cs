﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace livros
{
    public class Catalogo : ICatalogo
    {
        public List<Livro> GetLivros()
        {
            var livros = new List<Livro>();
            livros.Add(new Livro("001", "Tudo sobre Csharp", 99.99m));
            livros.Add(new Livro("002", "Vida de fullstack", 120.00m));
            livros.Add(new Livro("003", "Tudo sobre Javascript", 50.00m));
            return livros;
        }
    }
}
